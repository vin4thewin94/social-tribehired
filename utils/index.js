const mergeComments = (posts, comments) => {
    let final = []

    for( let i = 0; i < posts.length ; i++ ){
        let postComments = comments.filter(comment => comment.postId === posts[i].id)
        let post = {...posts[i], postComments}
        final.push(post)
    }

    let top = [...final].sort((a,b) => b.postComments.length - a.postComments.length).slice(0,5)

    let merge = {
        final,
        top
    }

    return merge
}


export { mergeComments }