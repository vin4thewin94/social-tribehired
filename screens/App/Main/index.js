import { createStackNavigator } from 'react-navigation-stack'

import Main from './Main'
import Comments from '../Comments'

const MainStack = createStackNavigator({
    Main,
    Comments
},{
    headerMode: 'none',
})

export default MainStack