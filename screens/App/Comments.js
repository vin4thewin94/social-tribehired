import React, { useContext } from 'react'
import PostsContext from '../../context/posts'

import Container from '../../components/Container'
import Header from '../../components/Header'
import List from '../../components/List'
import Post from '../../components/Post'

const Comments = (props) => {
    const posts = useContext(PostsContext)
    const { post } = props.navigation.state.params

    return (
        <Container>
            <Header title={'Comments'} />
            <Post post={post} />
            <List posts={post.postComments} renderPost={posts.renderComment} />
        </Container>
    )
}

export default Comments
