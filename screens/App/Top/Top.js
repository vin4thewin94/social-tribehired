import React, { useContext } from 'react'
import PostsContext from '../../../context/posts'

import Container from '../../../components/Container'
import Header from '../../../components/Header'
import List from '../../../components/List'


const Top = (props) => {
    const posts = useContext(PostsContext)  
    const { navigation } = props

    return (
        <Container>
            <Header title={'Top 5'} />
            <List posts={posts.top} icons renderPost={posts.renderPost} navigation={navigation} />
        </Container>
    )
}

export default Top