import { createStackNavigator } from 'react-navigation-stack'

import Top from './Top'
import Comments from '../Comments'

const TopStack = createStackNavigator({
    Top,
    Comments
},{
    headerMode: 'none',
})

export default TopStack