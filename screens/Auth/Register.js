import React, { useContext } from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import AuthContext from '../../context/auth'

import Container from '../../components/Container'
import Greeting from '../../components/Greeting'
import ErrorMessage from '../../components/ErrorMessage'
import Action from '../../components/Action'
import MessageLink from '../../components/MessageLink'
import { Form, Label, Input } from '../../components/Form'

const Register = (props) => {
    const auth = useContext(AuthContext)

    return (
        <Container justifyContent>
            <StatusBar barStyle='light-content' />
            <Greeting text='Sign Up Now!' />

            {
                auth.signUpErrorMessage
                ?
                    <ErrorMessage text={auth.signUpErrorMessage} />
                :   null
            }

            <Form>
                <View>
                    <Label>Name</Label>
                    <Input 
                        borderBottomWidth={StyleSheet.hairlineWidth}  
                        autoCapitalize='none' 
                        onChangeText={name => auth.setName(name)} 
                        value={auth.name}
                    />
                </View>

                <View style={{ marginTop: 32 }}>
                    <Label>Email</Label>
                    <Input 
                        borderBottomWidth={StyleSheet.hairlineWidth}  
                        autoCapitalize='none' 
                        onChangeText={email => auth.setEmail(email)} 
                        value={auth.email}
                    />
                </View>

                <View style={{ marginTop: 32 }}>
                    <Label>Password</Label>
                    <Input 
                        borderBottomWidth={StyleSheet.hairlineWidth} 
                        secureTextEntry 
                        autoCapitalize='none' 
                        onChangeText={password => auth.setPassword(password)} 
                        value={auth.password}
                    />
                </View>
            </Form>


            <Action text='Sign Up' link={() => auth.handleSignUp(props.navigation)} />
            <MessageLink line='Have an account?' action='Login' link={() => props.navigation.navigate('Login')} />
        </Container>
    )
}


export default Register