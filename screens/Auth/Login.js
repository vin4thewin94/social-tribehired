import React, { useContext } from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import AuthContext from '../../context/auth'

import Container from '../../components/Container'
import Greeting from '../../components/Greeting'
import ErrorMessage from '../../components/ErrorMessage'
import Action from '../../components/Action'
import MessageLink from '../../components/MessageLink'
import { Form, Label, Input } from '../../components/Form'


const Login = (props ) => {
    const auth = useContext(AuthContext)

    return (
        <Container justifyContent>
            <StatusBar barStyle='light-content' />
            <Greeting big text='SOCIAL' />

            {
                auth.loginErrorMessage
                ?
                    <ErrorMessage text={auth.loginErrorMessage} />
                :   null
            }
            

            <Form>
                <View>
                    <Label>Email</Label>
                    <Input 
                        borderBottomWidth={StyleSheet.hairlineWidth}  
                        autoCapitalize='none' 
                        onChangeText={email => auth.setEmail(email)} 
                        value={auth.email}
                    />
                </View>

                <View style={{ marginTop: 32 }}>
                    <Label>Password</Label>
                    <Input 
                        borderBottomWidth={StyleSheet.hairlineWidth} 
                        secureTextEntry 
                        autoCapitalize='none' 
                        onChangeText={password => auth.setPassword(password)} 
                        value={auth.password}
                    />
                </View>
            </Form>

            <Action text='Sign In' link={() => auth.handleLogin(props.navigation)} />
            <MessageLink line='New to Social?' action='Sign Up' link={() => props.navigation.navigate('Register')} />
        </Container>
    )
}


export default Login
