import React, { useContext, useEffect } from 'react'
import { ActivityIndicator } from 'react-native'
import AuthContext from '../context/auth'
import Container from '../components/Container'


const Loading = (props) => {
    const auth = useContext(AuthContext)

    useEffect(() => {
        // Check if user in memory
        props.navigation.navigate(auth.user.name ? "App": "Auth")
    }, [])

    return (
        <Container justifyContent alignItems>
            <ActivityIndicator size='large' />
        </Container>
    )
}

export default Loading
