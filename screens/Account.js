import React, { useContext } from 'react'
import AuthContext from '../context/auth'

import Container from '../components/Container'
import Action from '../components/Action'


const Account = (props) => {
    const auth = useContext(AuthContext)

    return (
        <Container justifyContent>
            <Action text='Logout' link={() => auth.logout(props.navigation)} />
        </Container>
    )
}

export default Account
