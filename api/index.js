class POSTS {
    static all = () => {
        return fetch('https://jsonplaceholder.typicode.com/posts/')
        .then( res => {
            return res.json()
        })
    }
}

class COMMENTS {
    static all = () => {
        return fetch('https://jsonplaceholder.typicode.com/comments/')
        .then( res => {
            return res.json()
        })
    }
}

export { POSTS, COMMENTS }