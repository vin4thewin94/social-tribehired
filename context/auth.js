import React from 'react'

export default React.createContext({
    user: {},
    email: '',
    password: '',
    name: '',
    loginErrorMessage: '',
    signUpErrorMessage: '',
    handleLogin: () => {},
    handleSignUp: () => {},
    logout: () => {}
})