import React from 'react'

export default React.createContext({
    posts: [],
    top: [],
    renderPost: () => {},
    renderComment: () => {}
})