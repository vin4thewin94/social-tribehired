import React, { useState, useEffect } from 'react'
import PostsContext from '../context/posts'
import Post from '../components/Post'
import Comment from '../components/Comment'

import { POSTS, COMMENTS } from '../api'
import { mergeComments } from '../utils'

const PostsProvider = (props) => {
  const [posts, setPosts] = useState([])
  const [top, setTop] = useState([])

  useEffect(() => {
    getPosts()
  }, [])

  getPosts = async () => {
    let posts = await POSTS.all()
    let comments = await COMMENTS.all()

    let merge = await mergeComments(posts, comments)

    let final = merge.final
    let top = merge.top


    setPosts(final)
    setTop(top)
  }

  renderPost = (post, navigation, icons) => {
    return (
      <Post post={post} navigation={navigation} icons={icons} />
    )
  }

  renderComment = (comment, navigation) => {
    return (
      <Comment comment={comment} navigation={navigation} />
    )
  }

  return (
    <PostsContext.Provider
      value={{
        posts,
        top,
        renderPost,
        renderComment
      }}
    >
      {props.children}
    </PostsContext.Provider>
  )
}

export default PostsProvider