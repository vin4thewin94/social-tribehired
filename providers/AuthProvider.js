import React, { useState, useEffect } from 'react'
import AuthContext from '../context/auth'

const AuthProvider = (props) => {
    const [user, setUser] = useState({})
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [loginErrorMessage, setloginErrorMessage] = useState('')
    const [signUpErrorMessage, setSignUpErrorMessage] = useState('')


    handleLogin = (navigation) => {
        console.log(email, password)
        setEmail('')
        setPassword('')

        navigation.navigate('App')
    }

    handleSignUp = (navigation) => {
        console.log(name, email, password)
        setName('')
        setEmail('')
        setPassword('')

        navigation.navigate('App')
    }

    logout = (navigation) => {
        setUser({})
        //Remove user from memory

        navigation.navigate('Login')
    }


    return (
        <AuthContext.Provider
            value={{
                user,
                email,
                password,
                name,
                setEmail,
                setPassword,
                setName,
                loginErrorMessage,
                signUpErrorMessage,
                handleLogin,
                handleSignUp,
                logout
            }}
        >
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthProvider