import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'

import PostsProvider from './providers/PostsProvider'
import AuthProvider from './providers/AuthProvider'



import Loading from './screens/Loading'
import Login from './screens/Auth/Login'
import Register from './screens/Auth/Register'

import HomeStack from './screens/App/Main/index'
import TopStack from './screens/App/Top/index'
import Account from './screens/Account'

const AuthStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false
        }
    },
    Register: {
        screen: Register,
        navigationOptions: {
            headerShown: false
        }
    },
})

const AppTabNavigation = createBottomTabNavigator({
    Main: {
        screen: HomeStack,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <Ionicons name='ios-home' size={24} color={tintColor} />
        }
    },
    Top: {
        screen: TopStack,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>(
                <MaterialCommunityIcons
                    name='numeric-5' 
                    size={55} 
                    color='#4EBFDE' 
                    style={{
                        shadowColor: '#4EBFDE',
                        shadowOffset: { width: 0, height: 0 },
                        shadowRadius: 10,
                        shadowOpacity: 0.3
                    }}
                />
            )
        }
    },
    Account: {
        screen: Account,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <MaterialCommunityIcons name='account' size={24} color={tintColor} />
        }
    }
},{
    tabBarOptions: {
        activeTintColor: '#161F3D',
        inactiveTintColor: '#BBBBC4',
        showLabel: false
    }
})


const AppContainer = createAppContainer(
    createSwitchNavigator({
        Loading,
        App: AppTabNavigation,
        Auth: AuthStack
    },{
        initialRouteName: "Loading"
    })
)

const App = () => {
  return (
        <AuthProvider>
            <PostsProvider>
                <AppContainer />
            </PostsProvider>
        </AuthProvider>
  )
}

export default App