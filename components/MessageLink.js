import React from 'react'
import styled from 'styled-components/native'

const Message = styled.TouchableOpacity`
    align-self: center;
    margin-top: 32
`

const Line = styled.Text`
    color: #414959;
    font-size: 13
`

const Action = styled.Text`
    font-weight: 500;
    color: #4EBFDE
`

const index = props => {
    return (
        <Message onPress={props.link}>
            <Line>{props.line} <Action>{props.action}</Action></Line>
        </Message>
    )
}

export default index
