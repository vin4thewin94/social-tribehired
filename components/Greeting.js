import React from 'react'
import styled from 'styled-components/native'

const Greeting = styled.Text`
    margin-bottom: 40;
    font-size: ${props => props.big ? '25' : '18'};    
    font-weight: 400;
    text-align: center
`

const index = props => {
    return (
        <Greeting big={props.big}>{props.text}</Greeting>
    )
}

export default index
