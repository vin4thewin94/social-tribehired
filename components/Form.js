import React from 'react'
import styled from 'styled-components/native'

const Form = styled.View`
    margin-bottom: 48;
    marginHorizontal: 30
`

const Label = styled.Text`
    color: #8A8F9E;
    font-size: 10;
    text-transform: uppercase
`

const Input = styled.TextInput`
    border-bottom-color: #8A8F9E;
    border-bottom-width: ${props => props.borderBottomWidth};
    height: 40;
    font-size: 15;
    color: #161F3D
`

export { Form, Label, Input }
