import React from 'react'
import { View } from 'react-native'
import styled from 'styled-components/native'

const FeedItem = styled.View`
    background-color: white;
    border-radius: 5;
    flex-direction: row;
    marginVertical: 8;
    padding-left: 8;
    padding-right: 8;
    padding-top: 8;
    padding-bottom: 8;
`

const Avatar = styled.View`
    width: 20;
    margin-top: 10;
    height: 20;
    border-radius: 10;
    margin-right: 16;
    background-color: grey;
    align-items: center;
    justify-content: center;
`

const AvatarText = styled.Text`
    color: white;
    font-size: 8;
`

const Header = styled.View`
    margin-top: 10;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-start
`

const Title = styled.Text`
    font-size: 14;
    font-weight: 500;
    color: #454D65;
`

const Body = styled.Text`
    margin-top: 2;
    font-size: 12;
    color: #838899;
`

const Comment = (props) => {
    const { comment } = props

    return (
        <FeedItem>
            <Avatar>
                <AvatarText>{comment.id}</AvatarText>
            </Avatar>
            <View style={{ flex: 1 }}>
                <Header>
                    <Title>{comment.name}</Title>
                    <Title>{comment.email}</Title>
                </Header>
                <Body>{comment.body}</Body>
            </View>
        </FeedItem>
    )
}

export default Comment
