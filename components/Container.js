import React from 'react'
import styled from 'styled-components/native'

const Container = styled.View`
    flex: 1;
    background-color: #EFECF4;
    ${props => props.justifyContent ? 'justify-content: center' : null}
    ${props => props.alignItems ? 'alignItems: center' : null}
`

export default Container
