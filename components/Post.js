import React from 'react'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import { View } from 'react-native'
import styled from 'styled-components/native'

const FeedItem = styled.View`
    background-color: white;
    border-radius: 5;
    flex-direction: row;
    marginVertical: 8;
    padding-left: 8;
    padding-right: 8;
    padding-top: 8;
    padding-bottom: 8;
`

const Avatar = styled.View`
    width: 36;
    margin-top: 10;
    height: 36;
    border-radius: 18;
    margin-right: 16;
    background-color: grey;
    align-items: center;
    justify-content: center;
`

const Header = styled.View`
    margin-top: 10;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-start
`

const Title = styled.Text`
    font-size: 20;
    font-weight: 500;
    color: #454D65;
`

const Body = styled.Text`
    margin-top: 10;
    font-size: 14;
    color: #838899;
`

const WhiteText = styled.Text`
    color: white
`

const Icons = styled.View`
    flex-direction: row;
    margin-top: 16;
    justify-content: flex-end
`

const Comments = styled.TouchableOpacity`
    margin-right: 16
`

const CommentsNum = styled.View`
    position: absolute;
    right: -10;
    top: -10;
    width: 20;
    height: 20;
    border-radius: 20;
    background-color: #DE8F4E;
    justify-content: center;
    align-items: center;
    z-index: 10
`

const Post = (props) => {
    const { post } = props

    return (
        <FeedItem>
            <Avatar>
                <WhiteText>{post.id}</WhiteText>
            </Avatar>
            <View style={{ flex: 1 }}>
                <Header>
                    <Title>{post.title}</Title>
                </Header>
                <Body>{post.body}</Body>

                {
                    props.icons
                    ?
                        <Icons>
                            <Ionicons name='ios-heart-empty' size={24} color='#73788B' style={{marginRight: 16}} />
                            <Comments onPress={() => props.navigation.navigate('Comments', {post})}>
                                <MaterialCommunityIcons name='comment' size={24} color='#73788B' />
                                <CommentsNum>
                                    <WhiteText>{post.postComments.length}</WhiteText>
                                </CommentsNum>
                            </Comments>
                        </Icons>
                    :
                        null
                }
            </View>
        </FeedItem>
    )
}

export default Post
