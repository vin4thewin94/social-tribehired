import React from 'react'
import styled from 'styled-components/native'

const Header = styled.View`
    padding-top: 64;
    padding-bottom: 16;
    background-color: #FFF;
    align-items: center;
    justify-content: center;
    border-bottom-width: 1;
    border-bottom-color: #EBECF4;
    shadow-color: #454D65;
    shadow-radius: 15;
    shadow-opacity: 0.2;
    z-index: 16
`

const Title = styled.Text`
    font-size: 20;
    font-weight: 500
`


const index = props => {
    return (
        <Header styles={{ shadowOffset: { height: 5 } }}>
            <Title testID="title">{props.title}</Title>
        </Header>
    )
}

export default index
