import React from 'react'
import styled from 'styled-components/native'

const List = styled.FlatList`
    margin-horizontal: 16;
`


const index = props => {
    const { navigation, icons } = props

    return (
        <List 
            data={props.posts} 
            renderItem={({item}) => props.renderPost(item, navigation, icons)} 
            keyExtractor={item => item.id.toString()}
            showsVerticalScrollIndicator={false}
        />
    )
}

export default index
