import React from 'react'
import styled from 'styled-components/native'

const Error = styled.View`
    height: 72;
    align-items: center;
    justify-content: center;
    marginHorizontal: 30
`

const Message = styled.Text`
    color:  #4EBFDE;
    font-size: 13;
    font-weight: 600;
    text-align: center
`

const index = props => {
    return (
        <Error>
            <Message>{props.text}</Message>
        </Error>
    )
}

export default index
