import React from 'react'
import styled from 'styled-components/native'

const Button = styled.TouchableOpacity`
    margin-horizontal: 30;
    background-color: #4EBFDE;
    border-radius: 4;
    height: 52;
    align-items: center;
    justify-content: center
`

const ButtonText = styled.Text`
    color: white;
    font-weight: 500
`

const index = props => {
    return (
        <Button onPress={props.link}>
            <ButtonText>{props.text}</ButtonText>
        </Button>
    )
}

export default index
