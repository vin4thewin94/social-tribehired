import { POSTS, COMMENTS } from '../../api'
import dummyComments from '../../dummyData/dummyComments'
import dummyPosts from '../../dummyData/dummyPosts.json'

describe('API', () => {
    describe('Posts API', () => {
        it('Retrieve Posts Response in Array', async () => {
            global.fetch = jest.fn().mockImplementation(() => {
                let promise = new Promise((resolve, reject) => {
                    resolve({
                        json: () => {
                            return dummyPosts
                        }
                    })
                })
                return promise
            })

            const response = await POSTS.all()
            expect(global.fetch).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/posts/')
            expect(response.length).toEqual(dummyPosts.length)
            expect(response[0]).toHaveProperty('id')
            expect(response[0]).toHaveProperty('userId')
            expect(response[0]).toHaveProperty('title')
            expect(response[0]).toHaveProperty('body')
        })
    })

    describe('Comments API', () => {
        it('Retrieve Comments Response in Array', async () => {
            global.fetch = jest.fn().mockImplementation(() => {
                let promise = new Promise((resolve, reject) => {
                    resolve({
                        json: () => {
                            return dummyComments
                        }
                    })
                })
                return promise
            })

            const response = await COMMENTS.all()
            expect(global.fetch).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/comments/')
            expect(response.length).toEqual(dummyComments.length)
            expect(response[0]).toHaveProperty('id')
            expect(response[0]).toHaveProperty('postId')
            expect(response[0]).toHaveProperty('name')
            expect(response[0]).toHaveProperty('email')
            expect(response[0]).toHaveProperty('body')
        })
    })
})