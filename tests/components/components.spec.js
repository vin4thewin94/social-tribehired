import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import 'jest-styled-components/native'
import Header from '../../components/Header'
import Post from '../../components/Post'
import Comment from '../../components/Comment'


describe('Components', () => {

    describe('Header', () => {
        it('renders a Header', () => {
            const wrapper = shallow(<Header title='Feed' />)
            expect(wrapper.text()).toEqual('Feed')
        })

        it('Header Snapshot', () => {
            const header = renderer.create(<Header title={'Feed'} />).toJSON()
            expect(header).toMatchSnapshot()
        })

    })

    describe('Post', () => {

        let post = {
            id: 1,
            title: '1233',
            body: '12345',
            postComments: []
        }

        it('renders a Post', () => {       
            const PostData = renderer.create(<Post post={post} />).root
            expect(PostData.props.post.id).toEqual(post.id)
            expect(PostData.props.post.title).toEqual(post.title)
            expect(PostData.props.post.body).toEqual(post.body)
            expect(PostData.props.post.postComments.length).toEqual(post.postComments.length)
        })

        it('renders a Post with icons', () => {
            const PostData = renderer.create(<Post post={post} icons />).root
            expect(PostData.props.icons).toEqual(true)
        })

        it('Post Snapshot', () => {
            const postSnap = renderer.create(<Post post={post} />).toJSON()
            expect(postSnap).toMatchSnapshot()
        })
    })

    describe('Comment', () => {
        let comment = {
            id: 1,
            name: '1233',
            email: 'email@email.io',
            body: '12345',
        }

        it('renders a Comment', () => {
            const CommentData = renderer.create(<Comment comment={comment} />).root
            expect(CommentData.props.comment.id).toEqual(comment.id)
            expect(CommentData.props.comment.name).toEqual(comment.name)
            expect(CommentData.props.comment.email).toEqual(comment.email)
            expect(CommentData.props.comment.body).toEqual(comment.body)
        })

        it('Comment Snapshot', () => {
            const commentSnap = renderer.create(<Comment comment={comment} />).toJSON()
            expect(commentSnap).toMatchSnapshot()
        })
    })
})







