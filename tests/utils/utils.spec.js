import { mergeComments } from '../../utils'
import dummyComments from '../../dummyData/dummyComments'
import dummyPosts from '../../dummyData/dummyPosts.json'

describe('Util', () => {
    describe('Merge Comments', () => {
        it('Merge Posts and Comments Data', async () => {
            let posts = dummyPosts
            let comments = dummyComments
            let merge = await mergeComments(posts, comments)

            expect(merge).toHaveProperty('final')
            expect(merge.final.length).toEqual(dummyPosts.length)

            expect(merge).toHaveProperty('top')
            expect(merge.top.length).toEqual(5)
        })
    })
})