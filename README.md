Tribe Hired Project: Social
============
## Brief
Build a React Native project that displays the top 5 posts with the most number of comments. Display the post's title, body, and the total number of comments the post received. 

1. comments endpoint
https://jsonplaceholder.typicode.com/comments

2. view post endpoint 
https://jsonplaceholder.typicode.com/posts/{post_id}

Do refactor your code yea, cause we wanna see how you structure your code.

Plus points if you follow a TDD approach as well.


## Commands
Start: yarn start

Test: yarn test


## EXPO Link
[Social](https://expo.io/@vin4thewin94/social)


